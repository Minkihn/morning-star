TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += object_parallel_to_source

SOURCES += main.cxx \
    Core/Application.cxx \
    Core/Console.cxx \
    Core/Managers/FontManager.cxx \
    Game/Context.cxx \
    Game/States/Init.cxx \
    Game/States/Menu.cxx \
    Core/Managers/MusicManager.cxx \
    Core/Managers/TextureManager.cxx \
    Game/Entities/Entity.cxx \
    Game/Entities/Ship.cxx \
    Game/Entities/Factory.cxx \
    Game/Entities/EnemyShip.cxx \
    Game/Widgets/Factory.cxx \
    Game/Widgets/Widget.cxx \
    Game/Widgets/Themable.cxx \
    Core/Managers/ThemeManager.cxx \
    Game/Controllers/AI.cxx \
    Game/Controllers/Keyboard.cxx \
    Game/Controllers/Mouse.cxx \
    Game/Controllers/Gamepad.cxx \
    Game/States/Play.cxx \
    Core/Renderer.cxx

HEADERS += Core/Application.hxx \
    Core/Console.hxx \
    Core/Singleton.hxx \
    Core/Events/Event.hxx \
    Core/Managers/FontManager.hxx \
    Game/Context.hxx \
    Game/State.hxx \
    Game/States/Init.hxx \
    Game/States/Menu.hxx \
    Core/Managers/MusicManager.hxx \
    Core/Managers/TextureManager.hxx \
    Core/Exceptions/MissingResource.hxx \
    Core/Exceptions/MissingFont.hxx \
    Game/Entities/Entity.hxx \
    Game/Entities/Ship.hxx \
    Core/Events/Pump.hxx \
    Game/Entities/Factory.hxx \
    Game/Entities/EnemyShip.hxx \
    Game/Widgets/Factory.hxx \
    Game/Widgets/Widget.hxx \
    Game/Widgets/Theme.hxx \
    Game/Widgets/Themable.hxx \
    Core/Managers/ThemeManager.hxx \
    Game/Controllers/Controller.hxx \
    Game/Controllers/AI.hxx \
    Game/Controllers/Gamepad.hxx \
    Game/Controllers/Mouse.hxx \
    Game/Controllers/Keyboard.hxx \
    Core/Publisher.hxx \
    Core/Subscriber.hxx \
    Game/States/Play.hxx \
    Core/Renderer.hxx

QMAKE_CXXFLAGS += -std=c++11
CONFIG         += c++11

macx {
    INCLUDEPATH   += /usr/local/include
    LIBS          += -L/usr/local/lib -lsfml-graphics -lsfml-system -lsfml-window -lsfml-audio
}

unix {
    INCLUDEPATH   += /usr/include
    LIBS          += -L/usr/lib -lsfml-graphics -lsfml-system -lsfml-window -lsfml-audio
}

cache()
