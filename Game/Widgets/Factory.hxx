#ifndef FACTORY_HXX
#define FACTORY_HXX

#include "Core/Singleton.hxx"

namespace game
{
    namespace widgets
    {
        class Factory : public core::Singleton<Factory>
        {
            public:
                friend class core::Singleton<Factory>;

            protected:
                Factory();
        };
    }
}

#endif // FACTORY_HXX
