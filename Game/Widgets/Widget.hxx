#ifndef WIDGET_HXX
#define WIDGET_HXX

#include "Themable.hxx"

namespace game
{
    namespace widgets
    {
        class Widget : public Themable
        {
            public:
                Widget();
        };
    }
}

#endif // WIDGET_HXX
