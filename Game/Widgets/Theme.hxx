#ifndef THEME_HXX
#define THEME_HXX

#include <SFML/Graphics/Color.hpp>

namespace game
{
    namespace widgets
    {
        struct Theme
        {
            sf::Color bgColor;
            sf::Color bgColorHover;
            sf::Color fgColor;
            sf::Color fgColorHover;
        };
    }
}

#endif // THEME_HXX
