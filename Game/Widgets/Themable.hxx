#ifndef THEMABLE_HXX
#define THEMABLE_HXX

#include "Theme.hxx"

namespace game
{
    namespace widgets
    {
        class Themable
        {
            public:
                virtual void doTheme() = 0;

            protected:
                Themable();
                Theme mTheme;
        };
    }
}

#endif // THEMABLE_HXX
