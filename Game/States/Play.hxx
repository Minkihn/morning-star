#ifndef PLAY_HXX
#define PLAY_HXX

#include "Game/State.hxx"
#include "Game/Controllers/Controller.hxx"
#include "Game/Entities/Entity.hxx"

namespace game
{
    namespace states
    {
        class Play : public game::State
        {
            public:
                Play(game::Context* c);

                ~Play();

                void update();

                void enable();

                void disable();

            private:
                game::controllers::Controller* mController;
                game::entities::Ship*          mPlayer;
        };
    }
}

#endif // PLAY_HXX
