#include "Play.hxx"

#include "Core/Console.hxx"
#include "Game/Controllers/Keyboard.hxx"
#include "Game/Controllers/Mouse.hxx"
#include "Game/Entities/Factory.hxx"

namespace game
{
    namespace states
    {
        Play::Play(Context* c) :
            State(c),
            mController( new controllers::Keyboard ),
            mPlayer( entities::Factory::getInstance()->create<entities::Ship>("ship") )
        {
            mController->setShip(mPlayer);

            mContext->subscribe(mController, sf::Event::KeyPressed);
            mContext->subscribe(mController, sf::Event::KeyReleased);
        }



        Play::~Play()
        {
            mContext->unsubscribe(mController, sf::Event::KeyPressed);
            mContext->unsubscribe(mController, sf::Event::KeyReleased);

            delete mController;
            delete mPlayer;
        }



        void Play::update()
        {
            mController->update();

            mPlayer->update();
        }



        void Play::enable()
        {
            core::Console::out(core::Console::DEBUG, "Play enabled.");

            mContext->addSprite(mPlayer->getSprite() );
        }



        void Play::disable()
        {
        }

    }
}

