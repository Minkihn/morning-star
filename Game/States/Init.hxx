#ifndef INIT_HXX
#define INIT_HXX

#include "Game/State.hxx"

namespace game
{
    namespace states
    {
        class Init : public game::State
        {
            public:
                Init(game::Context* c);
                ~Init();
                void update();
                void enable();
                void disable();
        };
    }
}

#endif // INIT_HXX
