#include "Init.hxx"

#include "Core/Console.hxx"
#include "Menu.hxx"

#include "Core/Managers/FontManager.hxx"
#include "Core/Managers/MusicManager.hxx"
#include "Core/Managers/TextureManager.hxx"
#include "Game/Entities/Factory.hxx"

#include "Game/Entities/Ship.hxx"
#include "Game/Entities/EnemyShip.hxx"

namespace game
{
    namespace states
    {
        Init::Init(Context* c) :
            State(c)
        {
            game::entities::Factory* fact = game::entities::Factory::getInstance();
            fact->learn<entities::Ship>("ship");
            fact->learn<entities::EnemyShip>("enemyShip");
        }

        Init::~Init()
        {
            core::managers::FontManager::destroyInstance();
            core::managers::MusicManager::destroyInstance();
            core::managers::TextureManager::destroyInstance();
            game::entities::Factory::destroyInstance();
        }

        void Init::update()
        {
            mContext->setCurrentState( new Menu(mContext) );
        }

        void Init::enable()
        {
            core::Console::out(core::Console::DEBUG, "Init enabled.");
        }

        void Init::disable()
        {
        }
    }
}

