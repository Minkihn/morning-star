#ifndef MENU_HXX
#define MENU_HXX

#include "Game/State.hxx"

namespace game
{
    namespace states
    {
        class Menu : public game::State
        {
            public:
                Menu(game::Context* c);
                void update();
                void enable();
                void disable();
        };
    }
}

#endif // MENU_HXX
