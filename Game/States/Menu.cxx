#include "Menu.hxx"

#include "Play.hxx"

#include "Core/Console.hxx"
#include "Core/Managers/MusicManager.hxx"

namespace game
{
    namespace states
    {
        Menu::Menu(Context* c) :
            State(c)
        {
        }

        void Menu::update()
        {
            mContext->setCurrentState( new Play(mContext) );
        }

        void Menu::enable()
        {
            core::Console::out(core::Console::DEBUG, "Menu enabled.");

            core::managers::MusicManager* mixer = core::managers::MusicManager::getInstance();
            mixer->pick("menu");
            mixer->play();
        }

        void Menu::disable()
        {

        }
    }
}

