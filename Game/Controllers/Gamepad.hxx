#ifndef GAMEPAD_HXX
#define GAMEPAD_HXX

#include "Controller.hxx"

namespace game
{
    namespace controllers
    {
        class Gamepad : public Controller
        {
            public:
                Gamepad();
        };
    }
}

#endif // GAMEPAD_HXX
