#ifndef KEYBOARD_HXX
#define KEYBOARD_HXX

#include <SFML/Window/Event.hpp>

#include "Controller.hxx"

namespace game
{
    namespace controllers
    {
        class Keyboard : public Controller
        {
            public:
                Keyboard();

                void update();

                void notify(const sf::Event& event) {}

            protected:
                void handlePressed();
                void handleReleased();
        };
    }
}
#endif // KEYBOARD_HXX
