#ifndef MOUSE_HXX
#define MOUSE_HXX

#include <SFML/Window/Event.hpp>

#include "Controller.hxx"


namespace game
{
    namespace controllers
    {
        class Mouse : public Controller
        {
            public:
                Mouse();

                void notify(const sf::Event& event);

            protected:
                void handlePressed(const sf::Event& event);

                void handleReleased(const sf::Event& event);

                void handleMoved(const sf::Event& event);
        };
    }
}

#endif // MOUSE_HXX
