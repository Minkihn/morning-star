#include "Keyboard.hxx"

namespace game
{
    namespace controllers
    {
        Keyboard::Keyboard()
        {
        }



        void Keyboard::update()
        {
            this->handlePressed();
            this->handleReleased();
        }



        void Keyboard::handlePressed()
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                mShip->strafeLeft();

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                mShip->strafeRight();

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
                mShip->moveForward();

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
                mShip->moveBackward();
        }



        void Keyboard::handleReleased()
        {
            if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
                    && !sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
                    && !sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
                    && !sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
                mShip->setVelocity( sf::Vector2i(0, 0) );
        }
    }
}
