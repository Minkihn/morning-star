#ifndef AI_HXX
#define AI_HXX

#include "Controller.hxx"

namespace game
{
    namespace controllers
    {
        class AI : public Controller
        {
            public:
                AI();
        };
    }
}

#endif // AI_HXX
