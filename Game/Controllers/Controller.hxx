#ifndef CONTROLLER_HXX
#define CONTROLLER_HXX

#include <SFML/Window/Event.hpp>

#include "Core/Subscriber.hxx"
#include "Game/Entities/Ship.hxx"

namespace game
{
    namespace controllers
    {
        class Controller : public core::Subscriber<sf::Event>
        {
            public:
                virtual ~Controller() {}

                virtual void update() = 0;

                virtual void setShip(entities::Ship* entity) {mShip = entity;}

            protected:
                Controller() : mShip(nullptr) {}

                entities::Ship* mShip;

        };
    }
}


#endif // CONTROLLER_HXX
