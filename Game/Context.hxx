#ifndef CONTEXT_HXX
#define CONTEXT_HXX

#include <map>
#include <list>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window/Event.hpp>

#include "State.hxx"

#include "Core/Renderer.hxx"
#include "Core/Publisher.hxx"
#include "Core/Events/Pump.hxx"

namespace game
{
    class State;

    typedef core::events::Pump<sf::Event, sf::Event::EventType> SFPump;
    typedef core::Publisher<sf::Event, sf::Event::EventType> SFPublisher;    

    class Context :
            public SFPump,
            public SFPublisher,
            public core::Renderer
    {
        public:
            Context();

            ~Context();

            void                setCurrentState(State* s);

            void                update();

        private:
            std::list<State*>   mStates;
            State*              mCurrentState;
    };
}

#endif // CONTEXT_HXX
