#ifndef ENTITY_HXX
#define ENTITY_HXX

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>

namespace game
{
    namespace entities
    {
        class Entity
        {
            public:
                Entity(const Entity& e);

                virtual ~Entity();

                virtual void            update();

                virtual void            strafeLeft();

                virtual void            strafeRight();

                virtual void            moveForward();

                virtual void            moveBackward();

                inline sf::Sprite*      getSprite()     const               {return mSprite;}
                inline float            getDirection()  const               {return mDirection;}
                inline sf::Vector2i     getVelocity()   const               {return mVelocity;}
                inline float            getMinSpeed()   const               {return mMinSpeed;}
                inline float            getMaxSpeed()   const               {return mMaxSpeed;}
                inline sf::Vector2i     getPosition()   const               {return mPosition;}
                inline sf::Vector2i     getSize()       const               {return mSize;}

//                 TODO: Handle texture switching (animated sprites?), but probably not this way.
//                 inline void             setSprite(sf::Sprite* s)            {mSprite = s;}
                inline void             setDirection(const float d)         {mDirection = d;}
                inline void             setVelocity(const sf::Vector2i& v)  {mVelocity  = v;}
                inline void             setMinSpeed(const float& s)         {mMinSpeed  = s;}
                inline void             setMaxSpeed(const float& s)         {mMaxSpeed  = s;}
                inline void             setPosition(const sf::Vector2i& p)  {mPosition  = p; mSprite->setPosition(mPosition.x, mPosition.y);}
                inline void             setSize(const sf::Vector2i& s)      {mSize      = s;}

            protected:
                Entity();

                sf::Sprite*             mSprite;

                float                   mDirection;
                float                   mMinSpeed;
                float                   mMaxSpeed;
                sf::Vector2i            mVelocity;
                sf::Vector2i            mPosition;
                sf::Vector2i            mSize;
        };
    }
}

#endif // ENTITY_HXX
