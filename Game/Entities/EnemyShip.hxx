#ifndef ENEMYSHIP_HXX
#define ENEMYSHIP_HXX

#include "Ship.hxx"

namespace game
{
    namespace entities
    {
        class EnemyShip : public Ship
        {
            public:
                EnemyShip();

                EnemyShip(const EnemyShip& es);
        };
    }
}

#endif // ENEMYSHIP_HXX
