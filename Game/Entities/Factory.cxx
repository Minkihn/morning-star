#include "Factory.hxx"

#include "Ship.hxx"

namespace game
{
    namespace entities
    {
        Factory::Factory()
        {
        }

        Factory::~Factory()
        {
            std::map<std::string, Entity*>::iterator itr;

            for (itr = mPrototypes.begin(); itr != mPrototypes.end(); ++itr)
                delete itr->second;

            mPrototypes.clear();
        }
    }
}
