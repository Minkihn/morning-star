#ifndef FACTORY_HXX
#define FACTORY_HXX

#include <string>
#include <map>

#include "Core/Console.hxx"
#include "Core/Singleton.hxx"
#include "Entity.hxx"

#include "Core/Managers/TextureManager.hxx"

namespace game
{
    namespace entities
    {
        class Factory : public core::Singleton<Factory>
        {
            public:
                friend class Singleton<Factory>;

                template<typename T>
                void learn(const std::string& name);

                template<typename T>
                T* create(const std::string& name);

            protected:
                Factory();
                ~Factory();

                template<typename T>
                void texturePrototype(const std::string& name, T* prototype);

                std::map<std::string, Entity*> mPrototypes;
        };



        template<typename T>
        void Factory::learn(const std::string& name)
        {
            std::map<std::string, Entity*>::const_iterator itr;

            T* prototype = new T();
            this->texturePrototype(name, prototype);

            if ( (itr = mPrototypes.find(name)) == mPrototypes.end())
                mPrototypes[name] = prototype;
        }



        template<typename T>
        T* Factory::create(const std::string& name)
        {
            std::map<std::string, Entity*>::const_iterator itr;

            T* t = nullptr;

            if ( (itr = mPrototypes.find(name)) != mPrototypes.end())
            {
                if (dynamic_cast<T*>(itr->second) )
                {
                    t = new T( *dynamic_cast<T*>(itr->second) );
                }
            }
            else
                core::Console::out(core::Console::WARNING, "Couldn't find prototype '" + name + "'.");

            return t;
        }



        template<typename T>
        void Factory::texturePrototype(const std::string& name, T* prototype)
        {
            core::managers::TextureManager *tm = core::managers::TextureManager::getInstance();

            prototype->getSprite()->setTexture( tm->get(name) );
        }
    }
}

#endif // FACTORY_HXX
