#ifndef SHIP_HXX
#define SHIP_HXX

#include "Entity.hxx"

namespace game
{
    namespace entities
    {
        class Ship : public Entity
        {
            public:
                Ship();

                Ship(const Ship& s);

                virtual ~Ship();

                inline bool getIsFiring() const         {return mIsFiring;}

                inline void setIsFiring(const bool& b)  {mIsFiring = b;}

            protected:
                bool        mIsFiring;
        };
    }
}

#endif // SHIP_HXX
