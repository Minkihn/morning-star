#include "Ship.hxx"

#include "Core/Console.hxx"

namespace game
{
    namespace entities
    {
        Ship::Ship() : Entity()
        {
        }



        Ship::Ship(const Ship& s) : Entity(s)
        {
        }



        Ship::~Ship()
        {
        }
    }
}
