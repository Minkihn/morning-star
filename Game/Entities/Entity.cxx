#include "Entity.hxx"

#include <cmath>

#define PI 3.141592653589793
#define UP 0
#define RIGHT 90
#define DOWN 180
#define LEFT 270

#include <SFML/Window/Mouse.hpp>

namespace game
{
    namespace entities
    {
        Entity::Entity() :
            mSprite(new sf::Sprite),
            mDirection( UP ),
            mMinSpeed( -12.0f ),
            mMaxSpeed( 12.0f ),
            mVelocity( sf::Vector2i(0, 0) ),
            mPosition( sf::Vector2i(0, 0) ),
            mSize( sf::Vector2i(32, 32) )
        {
        }



        Entity::Entity(const Entity& e)
        {
            mSprite     = new sf::Sprite(*e.getSprite()->getTexture());
            mDirection  = e.getDirection();;
            mMinSpeed   = e.getMinSpeed();
            mMaxSpeed   = e.getMaxSpeed();
            mVelocity   = sf::Vector2i(0, 0);
            mPosition   = sf::Vector2i(0, 0);
            mSize       = e.getSize();
        }



        Entity::~Entity()
        {
            delete mSprite;
        }



        void Entity::update()
        {
        }



        void Entity::strafeLeft()
        {
            sf::Vector2i pos = this->getPosition();

            if (mVelocity.x > mMinSpeed)
                mVelocity.x = -4.0f;

            pos.x += mVelocity.x * sin( (mDirection + 90.0f) * PI / 180.0f);
            pos.y += mVelocity.y * cos( (mDirection + 90.0f) * PI / 180.0f);

            this->setPosition(pos);
        }



        void Entity::strafeRight()
        {
            sf::Vector2i pos = this->getPosition();

            if (mVelocity.x < mMaxSpeed)
                mVelocity.x = 4.0f;

            pos.x += mVelocity.x * sin( (mDirection + 90.0f) * PI / 180.0f);
            pos.y += mVelocity.y * cos( (mDirection + 90.0f) * PI / 180.0f);

            this->setPosition(pos);
        }



        void Entity::moveForward()
        {
            sf::Vector2i pos = this->getPosition();

            if (mVelocity.y > mMinSpeed)
                mVelocity.y = -4.0f;

            pos.x += mVelocity.x * sin(mDirection * PI / 180.0f);
            pos.y += mVelocity.y * cos(mDirection * PI / 180.0f);

            this->setPosition(pos);
        }



        void Entity::moveBackward()
        {
            sf::Vector2i pos = this->getPosition();

            if (mVelocity.y < mMaxSpeed)
                mVelocity.y = 4.0f;

            pos.x += mVelocity.x * sin(mDirection * PI / 180.0f);
            pos.y += mVelocity.y * cos(mDirection * PI / 180.0f);

            this->setPosition(pos);
        }
    }
}
