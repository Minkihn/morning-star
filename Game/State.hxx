#ifndef STATE_HXX
#define STATE_HXX

#include "Context.hxx"

namespace game
{
    class Context;

    class State
    {
        public:
            State(Context* c) : mContext(c) {}
            virtual ~State()  {}
            virtual void update() = 0;
            virtual void enable() = 0;
            virtual void disable() = 0;

        protected:
            Context* mContext;
    };
}

#endif // STATE_HXX
