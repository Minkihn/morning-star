#include "Context.hxx"

#include "States/Init.hxx"
#include "States/Menu.hxx"

namespace game
{
    Context::Context() :
        mCurrentState( new game::states::Init(this) )
    {
        mStates.push_back( mCurrentState );
        mCurrentState->enable();
    }

    Context::~Context()
    {
        std::list<State*>::iterator itr;

        for (itr = mStates.begin(); itr != mStates.end(); )
        {
            delete *itr;
            itr = mStates.erase(itr);
        }
    }

    void Context::setCurrentState(State* s)
    {
        mCurrentState->disable();

        bool found = false;
        std::list<State*>::iterator itr;

        for (itr = mStates.begin(); itr != mStates.end(); ++itr)
        {
            if (*itr == s)
                found = true;
        }

        if (!found)
            mStates.push_back(s);

        mCurrentState = s;
        mCurrentState->enable();
    }

    void Context::update()
    {
        if (mEvents.size() > 0)
        {
            this->publish(mEvents.front());
            mEvents.pop_front();
        }

        mCurrentState->update();
    }
}
