#ifndef CONSOLE_HXX
#define CONSOLE_HXX

#include <iostream>

#include "Singleton.hxx"

namespace core
{
    class Console : public Singleton<Console>
    {
        public:
            friend class Singleton<Console>;

            enum Level
            {
                OFF     = 0,
                DEBUG   = 1,
                WARNING = 2,
                ERROR   = 4,
                FATAL   = 8,
                ALL     = 15
            };

            const int&  getLevel()      {return mLevel;}

            void        setLevel(int l) {mLevel = l;}

            template<typename T>
            static void out(const int& l, const T& message);

        private:
            Console();

            void        prefix(const int& l);

            int         mLevel;
    };



    template<typename T>
    void Console::out(const int& l, const T& message)
    {
        const int& level = Console::getInstance()->getLevel();

        if (level > OFF)
        {
            if ( (level & l) || level == ALL )
            {
                Console::getInstance()->prefix(l);

                if (l != OFF && l != ALL)
                    std::cout << message << std::endl;
            }
        }
    }
}

#endif // CONSOLE_HXX
