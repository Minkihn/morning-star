#ifndef MISSINGFONT_HXX
#define MISSINGFONT_HXX

#include "MissingResource.hxx"

namespace core
{
    namespace exceptions
    {
        class MissingFont : public MissingResource
        {
            public:
                MissingFont(const std::string& name) throw()
                    : MissingResource("Font", name)
                {
                }
        };
    }
}

#endif // MISSINGFONT_HXX
