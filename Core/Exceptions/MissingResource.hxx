#ifndef MISSINGRESOURCE_HXX
#define MISSINGRESOURCE_HXX

#include <exception>
#include <string>

namespace core
{
    namespace exceptions
    {
        class MissingResource : public std::exception
        {
            public:
                virtual const char* what() const throw()
                {
                    return std::string("Missing Resource: " + this->getType() + ", " + this->getName()).c_str();
                }

                const std::string& getType() const throw() {return mType;}
                const std::string& getName() const throw() {return mName;}

            protected:
                MissingResource(const std::string& type, const std::string& name) throw()
                    : mType(type), mName(name)
                {
                }
                MissingResource() throw() {}

                std::string mName;
                std::string mType;
        };
    }
}

#endif // MISSINGRESOURCE_HXX
