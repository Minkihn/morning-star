#ifndef SUBSCRIBER_HXX
#define SUBSCRIBER_HXX

namespace core
{
    template<typename Subject>
    class Subscriber
    {
        public:
            virtual ~Subscriber() {}

            virtual void notify(const Subject& event) = 0;
    };
}

#endif // SUBSCRIBER_HXX
