#ifndef SINGLETON_HXX
#define SINGLETON_HXX

namespace core
{
    template<typename T>
    class Singleton
    {
        public:
            static T*   getInstance();
            static void destroyInstance();

        protected:
            Singleton()                     {}
            Singleton(const T&)             {}
            Singleton& operator=(const T&)  {}
            virtual ~Singleton()            {}

            static T*   mInstance;
    };

    template<typename T> T* Singleton<T>::mInstance = nullptr;

    template<typename T> T* Singleton<T>::getInstance()
    {
        if (mInstance == nullptr)
            mInstance = new T();
        return mInstance;
    }

    template<typename T> void Singleton<T>::destroyInstance()
    {
        if (mInstance != nullptr)
        {
            delete mInstance;
            mInstance = nullptr;
        }
    }
}

#endif // SINGLETON_HXX
