#include "Renderer.hxx"

namespace core
{
    void Renderer::draw()
    {
        mRenderTarget->clear();

        for (std::list<sf::Sprite*>::const_iterator it = mSprites.begin(); it != mSprites.end(); ++it)
        {
            mRenderTarget->draw(**it);
        }
    }

}
