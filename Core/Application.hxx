#ifndef APPLICATION_HXX
#define APPLICATION_HXX

#include <SFML/Graphics/RenderWindow.hpp>

#include "Game/Context.hxx"

namespace core
{
    class Application
    {
        public:
            Application();
            int run();

        private:
            void update();
            // void draw();

            sf::RenderWindow    mWindow;
            bool                mWindowClosed;

            game::Context       mContext;
            bool                mContextStopped;
    };
}

#endif // APPLICATION_HXX
