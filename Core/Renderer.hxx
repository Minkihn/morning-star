#ifndef RENDERER_HXX
#define RENDERER_HXX

#include <list>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace core
{
    class Renderer
    {
        public:
            void                    setRenderTarget(sf::RenderTarget* target) {mRenderTarget = target;}
            void                    addSprite(sf::Sprite* s)                  {mSprites.push_back(s);}
            void                    draw();

        protected:
            Renderer()              {}
            std::list<sf::Sprite*>  mSprites;
            sf::RenderTarget*       mRenderTarget;
    };
}

#endif // RENDERER_HXX
