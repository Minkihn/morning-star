#ifndef PUBLISHER_HXX
#define PUBLISHER_HXX

#include <map>
#include <list>

#include "Subscriber.hxx"

namespace core
{
    template<typename Subject, typename SubjectType>
    class Publisher
    {
        public:
            virtual void subscribe(Subscriber<Subject>* subscriber, SubjectType subjectType);

            virtual void unsubscribe(Subscriber<Subject>* subscriber, SubjectType subjectType);

        protected:
            Publisher() {}
            virtual ~Publisher();

            void publish(const Subject& e);

            std::map< SubjectType, std::list<Subscriber<Subject>*> > mSubscriptions;
    };



    template<typename Subject, typename SubjectType>
    void Publisher<Subject, SubjectType>::subscribe(Subscriber<Subject>* subscriber,
                                                    SubjectType subjectType)
    {
        typename std::map< SubjectType, std::list<Subscriber<Subject>*> >::iterator itr;

        if ((itr = mSubscriptions.find(subjectType)) == mSubscriptions.end())
            mSubscriptions[subjectType].push_back(subscriber);
    }



    template<typename Subject, typename SubjectType>
    void Publisher<Subject, SubjectType>::unsubscribe(Subscriber<Subject>* subscriber,
                                                      SubjectType subjectType)
    {
        for (auto itl = mSubscriptions[subjectType].begin(); itl != mSubscriptions[subjectType].end(); )
        {
            if (*itl == subscriber)
            {
                itl = mSubscriptions[subjectType].erase(itl);
                break;
            }
            else
                ++itl;
        }
    }



    template<typename Subject, typename SubjectType>
    Publisher<Subject, SubjectType>::~Publisher()
    {
        mSubscriptions.clear();
    }



    template<typename Subject, typename SubjectType>
    void Publisher<Subject, SubjectType>::publish(const Subject& e)
    {
        typename std::map< SubjectType, std::list<Subscriber<Subject>*> >::iterator itm;
        typename std::list<Subscriber<Subject>*>::iterator itl;

        if ( (itm = mSubscriptions.find(e.type)) != mSubscriptions.end())
        {
            for (itl = itm->second.begin(); itl != itm->second.end(); ++itl)
            {
                (*itl)->notify(e);
            }
        }
    }
}

#endif // PUBLISHER_HXX
