#ifndef TEXTUREMANAGER_HXX
#define TEXTUREMANAGER_HXX

#include <map>
#include <string>
#include <SFML/Graphics/Texture.hpp>
#include "../../Core/Singleton.hxx"

namespace core
{
    namespace managers
    {
        class TextureManager : public Singleton<TextureManager>
        {
            public:
                friend class Singleton<TextureManager>;

                sf::Texture& get(const std::string& name) const;

            protected:
                TextureManager();
                ~TextureManager();

                std::map<std::string, sf::Texture*> mTextures;
        };
    }
}


#endif // TEXTUREMANAGER_HXX
