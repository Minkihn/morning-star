#include "TextureManager.hxx"

#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>

#include <SFML/Graphics/Image.hpp>

#include "Core/Console.hxx"

namespace core
{
    namespace managers
    {
        sf::Texture& TextureManager::get(const std::string& name) const
        {
            std::map<std::string, sf::Texture*>::const_iterator itr;

            Console::out(Console::DEBUG, "Fetching texture for keyword " + name);

            if ((itr = mTextures.find(name)) != mTextures.end() )
            {
                Console::out(Console::DEBUG, "Found texture for keyword " + name);
                return *itr->second;
            }
            else
            {
                Console::out(Console::DEBUG, "Found no texture for keyword " + name);
                // FIXME: Not sure if I should throw an exception instead.
                return *mTextures.find("blank")->second;
            }
        }



        TextureManager::TextureManager()
        {
            sf::Image img;
            img.create(32, 32, sf::Color(255, 192, 203));

            sf::Texture* blank = new sf::Texture();
            blank->loadFromImage(img);
            blank->setRepeated(true);
            mTextures["blank"] = blank;

            QFile file("Data/Settings/textures.json");

            if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                core::Console::out(core::Console::ERROR, "Failed loading textures.json.");
                return ;
            }

            QByteArray data = file.readAll();
            file.close();

            QJsonParseError err;
            QJsonDocument doc = QJsonDocument::fromJson(data, &err);

            if (err.error)
            {
                core::Console::out(core::Console::ERROR, err.errorString().toStdString());
                return ;
            }

            if (doc.isArray())
            {
                QJsonArray arr = doc.array();
                QJsonArray::iterator itr;

                for (itr = arr.begin(); itr != arr.end(); ++itr)
                {
                    if ((*itr).isObject())
                    {
                        QJsonObject obj = (*itr).toObject();

                        sf::Texture* texture = new sf::Texture();

                        std::string f  = "Data/Textures/" + obj["file"].toString().toStdString();
                        int x = obj["x"].toString().toUInt();
                        int y = obj["y"].toString().toUInt();
                        int w = obj["w"].toString().toUInt();
                        int h = obj["h"].toString().toUInt();

                        if ( !texture->loadFromFile(f, sf::IntRect( sf::Vector2i(x, y), sf::Vector2i(w, h) )) )
                        {
                            delete texture;
                            Console::out(Console::ERROR, "Couldn't load a texture.");
                        }
                        else
                            mTextures[ obj["name"].toString().toStdString() ] = texture;
                    }
                }
            }
        }

        TextureManager::~TextureManager()
        {
            std::map<std::string, sf::Texture*>::iterator itr;

            for (itr = mTextures.begin(); itr != mTextures.end(); ++itr)
                delete itr->second;
        }
    }
}
