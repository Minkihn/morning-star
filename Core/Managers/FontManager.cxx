#include "FontManager.hxx"

#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>

#include "Core/Console.hxx"
#include "Core/Exceptions/MissingFont.hxx"

namespace core
{
    namespace managers
    {
        sf::Font& FontManager::get(const std::string& name) const
        {
            std::map<std::string, sf::Font*>::const_iterator itr;

            if ((itr = mFonts.find(name)) != mFonts.end() )
                return *itr->second;
            else
                throw exceptions::MissingFont(name);
        }

        FontManager::FontManager()
        {
            QFile file("Data/Settings/fonts.json");

            if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                core::Console::out(core::Console::ERROR, "Failed loading fonts.json.");

            QByteArray data = file.readAll();
            file.close();

            QJsonParseError err;
            QJsonDocument doc = QJsonDocument::fromJson(data, &err);

            if (err.error)
                core::Console::out(core::Console::ERROR, err.errorString().toStdString());

            if (doc.isArray())
            {
                QJsonArray arr = doc.array();
                QJsonArray::iterator itr;

                for (itr = arr.begin(); itr != arr.end(); ++itr)
                {
                    if ((*itr).isObject())
                    {
                        QJsonObject obj = (*itr).toObject();

                        sf::Font* font = new sf::Font();
                        if (!font->loadFromFile("Data/Fonts/" + obj["file"].toString().toStdString()))
                        {
                            delete font;
                            Console::out(Console::ERROR, "Couldn't load a font file.");
                        }
                        else
                            mFonts[ obj["name"].toString().toStdString() ] = font;
                    }
                }
            }
        }

        FontManager::~FontManager()
        {
            std::map<std::string, sf::Font*>::iterator itr;

            for (itr = mFonts.begin(); itr != mFonts.end(); ++itr)
                delete itr->second;
        }
    }
}
