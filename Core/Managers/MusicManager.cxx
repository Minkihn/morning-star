#include "MusicManager.hxx"

#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>

#include "../Console.hxx"

namespace core
{
    namespace managers
    {
        void MusicManager::pick(unsigned int& id)
        {
            this->stop();

            if (id < mMusics.size())
                mCurrentMusic = id;
        }

        void MusicManager::pick(const std::string& name)
        {
            std::map<std::string, unsigned int>::iterator itr;

            this->stop();

            if ((itr = mIds.find(name)) != mIds.end())
                mCurrentMusic = mIds[name];
        }

        void MusicManager::play()
        {
            if (mMusics[ mCurrentMusic ] != nullptr)
            {
                mMusics[ mCurrentMusic ]->setVolume(100);
                mMusics[ mCurrentMusic ]->play();
            }
        }

        void MusicManager::pause()
        {
            mMusics[ mCurrentMusic ]->pause();
        }

        void MusicManager::stop()
        {
            if (mMusics[ mCurrentMusic ] != nullptr)
                mMusics[ mCurrentMusic ]->stop();
        }

        MusicManager::MusicManager() :
            mMusics( std::vector<sf::Music*>(10) ),
            mCurrentMusic(0)
        {
            QFile file("Data/Settings/musics.json");

            if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                Console::out(Console::ERROR, "Failed loading musics.json.");
                return ;
            }

            QByteArray data = file.readAll();
            file.close();

            QJsonParseError err;
            QJsonDocument doc = QJsonDocument::fromJson(data, &err);

            if (err.error)
            {
                Console::out(Console::ERROR, err.errorString().toStdString());
                return ;
            }
            else
            {
                if (doc.isArray())
                {
                    QJsonArray arr = doc.array();
                    QJsonArray::iterator itr;

                    for (itr = arr.begin(); itr != arr.end(); ++itr)
                    {
                        if ((*itr).isObject())
                        {
                            QJsonObject obj = (*itr).toObject();

                            sf::Music* mus = new sf::Music();
                            if (!mus->openFromFile("Data/Musics/" + obj["file"].toString().toStdString()))
                            {
                                delete mus;
                                Console::out(Console::ERROR, "Couldn't load a music file.");
                            }
                            else
                            {
                                mMusics[ obj["id"].toString().toUInt() ] = mus;
                                mIds[ obj["name"].toString().toStdString() ]  = obj["id"].toString().toUInt();
                            }
                        }
                    }
                }
            }
        }

        MusicManager::~MusicManager()
        {
            std::vector<sf::Music*>::iterator itr;

            for (itr = mMusics.begin(); itr != mMusics.end(); )
            {
                delete *itr;
                itr = mMusics.erase(itr);
            }
        }
    }
}
