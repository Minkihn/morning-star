#ifndef FONTMANAGER_HXX
#define FONTMANAGER_HXX

#include <map>
#include <string>
#include <SFML/Graphics/Font.hpp>
#include "../Singleton.hxx"

namespace core
{
    namespace managers
    {
        class FontManager : public Singleton<FontManager>
        {
            public:
                friend class Singleton<FontManager>;

                sf::Font& get(const std::string& name) const;

            private:
                FontManager();
                ~FontManager();

                std::map<std::string, sf::Font*> mFonts;
        };
    }
}

#endif // FONTMANAGER_HXX
