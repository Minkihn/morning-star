#ifndef THEMEMANAGER_HXX
#define THEMEMANAGER_HXX

#include <string>
#include <map>

#include "Core/Singleton.hxx"
#include "Game/Widgets/Theme.hxx"

namespace core
{
    namespace managers
    {
        class ThemeManager : public Singleton<ThemeManager>
        {
            public:
                friend class Singleton<ThemeManager>;
                game::widgets::Theme& get(const std::string& name);

            protected:
                ThemeManager();
                ~ThemeManager();

                std::map<std::string, game::widgets::Theme*> mThemes;
        };
    }
}



#endif // THEMEMANAGER_HXX
