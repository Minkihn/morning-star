#ifndef MUSICMANAGER_HXX
#define MUSICMANAGER_HXX

#include <string>
#include <vector>
#include <map>
#include <SFML/Audio/Music.hpp>

#include "../Singleton.hxx"

namespace core
{
    namespace managers
    {
        class MusicManager : public Singleton<MusicManager>
        {
            public:
                friend class Singleton<MusicManager>;

                void pick(unsigned int& id);
                void pick(const std::string& name);
                void play();
                void pause();
                void stop();

            private:
                MusicManager();
                ~MusicManager();

                std::vector<sf::Music*>             mMusics;
                std::map<std::string, unsigned int> mIds;
                unsigned int                        mCurrentMusic;
        };
    }
}

#endif // MUSICMANAGER_HXX
