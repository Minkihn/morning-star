#include "Console.hxx"



namespace core
{
    Console::Console()
    {
        mLevel = Console::ALL;
    }



    void Console::prefix(const int& l)
    {
        switch (l)
        {
            case DEBUG:    std::cout << "DEBUG: ";     break;
            case WARNING:  std::cout << "WARNING: ";   break;
            case ERROR:    std::cout << "ERROR: ";     break;
            case FATAL:    std::cout << "FATAL: ";     break;
            default: break;
        }
    }
}
