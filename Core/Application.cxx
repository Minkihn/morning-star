#include "Application.hxx"

#include <SFML/Window/Event.hpp>

#include "Console.hxx"

namespace core
{
    Application::Application() :
        mWindowClosed(false),
        mContextStopped(false)
    {
        mWindow.create(sf::VideoMode(600, 400), "Morning Star");

        Console::out(Console::DEBUG, "Application created.");
    }



    int Application::run()
    {
        mWindow.setVisible(true);
        mWindow.setFramerateLimit(60);
        mWindow.setKeyRepeatEnabled(true);
        // mWindow.setMouseCursorVisible(false);
        mContext.setRenderTarget(&mWindow);

        while (!mWindowClosed && !mContextStopped)
        {
            this->update();
            mContext.update();
            mContext.draw();
            mWindow.display();
        }

        mWindow.close();

        return 0;
    }



    void Application::update()
    {
        sf::Event e;

        while (mWindow.pollEvent(e))
        {
            if (e.type == sf::Event::Closed || (e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Escape))
                mWindowClosed = true;

            mContext.inject(e);
        }
    }



/*
    void Application::draw()
    {
        mWindow.clear();

        // Draw entities

        mWindow.display();
    }
*/
}
