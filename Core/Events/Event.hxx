#ifndef EVENT_HXX
#define EVENT_HXX

namespace core
{
    namespace events
    {
        class Event
        {
            public:
                struct ButtonEvent
                {
                        unsigned int identifier;
                };

                enum class EventType
                {
                    Clicked,
                    FocusGained,
                    FocusLost,
                    MouseOver
                };

                EventType type;

                union
                {
                        ButtonEvent button;
                };
        };
    }
}

#endif // EVENT_HXX
