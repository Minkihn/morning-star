#ifndef PUMP_HXX
#define PUMP_HXX

#include <list>

namespace core
{
    namespace events
    {
        template<typename Event, typename EventType>
        class Pump
        {
            public:
                void inject(const Event& e);

                bool pump(Event& e);

            protected:
                std::list<Event> mEvents;
        };

        template<typename Event, typename EventType>
        void Pump<Event, EventType>::inject(const Event& e)
        {
            mEvents.push_back(e);
        }

        template<typename Event, typename EventType>
        bool Pump<Event, EventType>::pump(Event& e)
        {
            if (mEvents.size() > 0)
            {
                e = mEvents.front();
                mEvents.pop_front();
                return true;
            }

            return false;
        }
    }
}

#endif // PUMP_HXX
