#include "Core/Application.hxx"
#include "Core/Console.hxx"

int main()
{
    core::Console::getInstance()->setLevel(core::Console::ALL); // | core::Console::DEBUG...

    return core::Application().run();
}
